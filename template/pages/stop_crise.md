# Crises monétaires

## Origine systémique

- [5:30 de vidéo : Le système bancaire pour les nuls](https://youtu.be/MfsbbsUunng)
- [En texte : L'île des naufragés](http://www.versdemain.org/articles/credit-social/item/l-ile-des-naufrages)
- [Dans la TRM : zoom sur l'histoire de la monnaie](http://trm.creationmonetaire.info/zooms-sur-l-histoire-de-la-monnaie.html)


## Comment faire mieux ?

Le principe de [symétrie](#symetrie_DU) est conçu pour ça.
En répartissant la création monétaire dans le temps et sur l'ensemble de la population,
il n'y a pas de captation systémique de la monnaie par les instances qui la créent,
donc pas d'assèchement monétaire, donc pas de crise.

Et avec des [règles de fonctionnement publiques et vérifiables](#page_libre), la confiance peut naître et perdurer.
